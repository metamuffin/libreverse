
// Reverses a list of any type.
export function reverse<T>(list: T[]): T[] {
    return [...list].reverse()
}
