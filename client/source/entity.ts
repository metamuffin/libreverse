import { BufferGeometry, Color, Group, Mesh, Object3D, PerspectiveCamera, PointLight } from "three";
import { IPartialEntity, IPosition, IRotation, IScale } from "../../common/packet";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"
import { active_camera, renderer, scene, set_active_camera } from ".";
import { traverse_group } from "./helper";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"
import { PatchableEntity } from "../../common/animation";

export abstract class ClientEntity implements PatchableEntity {
    static by_object_uuid: Map<string, ClientEntity> = new Map()
    static by_id: Map<string, ClientEntity> = new Map()

    id: string

    abstract object: Object3D
    clickable: boolean = true

    constructor(p: IPartialEntity) {
        this.id = p.id
    }

    static spawn(p: IPartialEntity): ClientEntity {
        if (p.type == "camera") return new ClientCamera(p)
        if (p.type == "light") return new ClientLight(p)
        if (p.type == "model") return new ClientModel(p)
        throw new Error("no");
    }

    apply_update(p: IPartialEntity) {
        if (p.position !== undefined) this.object.position.set(p.position.x, p.position.y, p.position.z)
        if (p.scale !== undefined) this.object.scale.set(p.scale.x, p.scale.y, p.scale.z)
        if (p.rotation !== undefined) this.object.rotation.set(p.rotation.x, p.rotation.y, p.rotation.z)
        if (p.clickable !== undefined) this.clickable = p.clickable
    }

    async load(p: IPartialEntity) {
        this.apply_update(p)
        scene.add(this.object)
        ClientEntity.by_id.set(this.id, this)
        ClientEntity.by_object_uuid.set(this.object.uuid, this)
    }

    async unload() {
        scene.remove(this.object)
        ClientEntity.by_id.delete(this.id)
        ClientEntity.by_object_uuid.delete(this.object.uuid)
    }
}

export class ClientModel extends ClientEntity {
    path!: string
    object = new Group()

    apply_update(p: IPartialEntity) {
        super.apply_update(p)
        if (p.path !== undefined) this.path = p.path
    }

    async load(p: IPartialEntity) {
        if (!p.path) throw new Error("path required");
        console.log(`loading ${p.path}.`);
        const model = await new GLTFLoader().loadAsync(p.path)
        console.log(`loaded ${p.path}.`);
        this.object = model.scene
        await super.load(p)
    }
}

export class ClientLight extends ClientEntity {
    object = new PointLight()
    apply_update(p: IPartialEntity) {
        super.apply_update(p)
        if (p.color !== undefined) this.object.color.set(p.color)
        if (p.intensity !== undefined) this.object.intensity = p.intensity
    }
}

export class ClientCamera extends ClientEntity {
    object: PerspectiveCamera = new PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.01, 100);

    apply_update(p: IPartialEntity) {
        super.apply_update(p)
    }
    async load(p: IPartialEntity) {
        set_active_camera(this.object)
        let oc = new OrbitControls(this.object, renderer.domElement)

        const u = () => {
            oc.update()
            requestAnimationFrame(u)
        }
        await super.load(p)
    }
}
