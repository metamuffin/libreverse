import { Animation } from "../../common/animation"
import { PAnimateEntity } from "../../common/packet";
import { ClientEntity } from "./entity";

export class ClientAnimation extends Animation {
    target: ClientEntity

    constructor(p: PAnimateEntity) {
        super(p)
        const e = ClientEntity.by_id.get(this.points[0].value.id)
        if (!e) throw new Error("entity to animate doesn't exist (yet)");
        this.target = e
    }
}

