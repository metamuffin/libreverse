
export class YourMomIsNotImplementedError extends Error { name = "YourMomIsNotImplementedError"; }
export class IDontKnowWhatToDoHereError extends Error { name = "IDontKnowWhatToDoHereError"; }
export class PleaseImplementThisError extends Error { name = "PleaseImplementThisError"; }
