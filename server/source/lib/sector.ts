import { Client } from "./client";
import { Entity, EntityContainer } from "./entity";

export class Sector implements EntityContainer {

    clients: Set<Client> = new Set()
    entities: Set<Entity> = new Set()

    needs_broadcast: Set<Entity> = new Set()
    broadcast_scheduled: boolean = false

    animations: 

    constructor() {

    }

    schedule_broadcast(e: Entity): void {
        if (!this.needs_broadcast.has(e))
            this.needs_broadcast.add(e)
        if (!this.broadcast_scheduled) {
            this.broadcast_scheduled = true
            setImmediate(() => {
                this.broadcast_scheduled = false

            })
        }
    }


}
